/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.utils;

import com.mycompany.radio.car.simulator.enumeration.Command;
import com.mycompany.radio.car.simulator.enumeration.Direction;
import com.mycompany.radio.car.simulator.exception.CommandExecutionFailedException;
import com.mycompany.radio.car.simulator.exception.CommandUnrecognizedException;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author emile
 */
public class CommandHelper {
    private static final int STEP = 1;
    
    private Rectangle2D occupation;
    private final Dimension dimension;
    private Direction direction;

    public CommandHelper(Rectangle2D occupation, Dimension dimension, Direction direction) {
        this.occupation = occupation;
        this.dimension = dimension;
        this.direction = direction;
    }
    
    public Rectangle2D getOccupation() {
        return occupation;
    }
    
    public Direction getDirection() {
        return direction;
    }
    
    public void next(Command command) {
        switch (command) {
            case RIGHT:
            case LEFT:
                nextDir(command);
                break;
            case FORWARD:
            case BACK:
                nextRect(command);
                break;
            default:
                throw new CommandUnrecognizedException(command);
        }
    }
    
    private void nextDir(Command command) {
       // if ((Direction.NORTH.equals(direction) && Command.RIGHT.equals(command))
        //        || (Direction.SOUTH.equals(direction) && Command.LEFT.equals(command)))
        switch (command) {
            case RIGHT:
                direction = Direction.rotateClockwise(direction);
                break;
            case LEFT:
                direction = Direction.rotateAnticlockwise(direction);
                break;
            default:
                throw new CommandUnrecognizedException(command);
        }
    }
    
    private void nextRect(Command command) {
        if ((Direction.EAST.equals(direction) && Command.FORWARD.equals(command))
                || (Direction.WEST.equals(direction) && Command.BACK.equals(command)))
            occupation = new Rectangle(new Point((int)occupation.getX()+STEP, (int)occupation.getY()), dimension);
        
        else if ((Direction.EAST.equals(direction) && Command.BACK.equals(command))
                || (Direction.WEST.equals(direction) && Command.FORWARD.equals(command)))
            occupation = new Rectangle(new Point((int)occupation.getX()-STEP, (int)occupation.getY()), dimension);
        
        else if ((Direction.NORTH.equals(direction) && Command.FORWARD.equals(command))
                || (Direction.SOUTH.equals(direction) && Command.BACK.equals(command)))
            occupation = new Rectangle(new Point((int)occupation.getX(), (int)occupation.getY()+STEP), dimension);
        
        else if ((Direction.NORTH.equals(direction) && Command.BACK.equals(command))
                || (Direction.SOUTH.equals(direction) && Command.FORWARD.equals(command)))
            occupation = new Rectangle(new Point((int)occupation.getX(), (int)occupation.getY()-STEP), dimension);
        
        else
            throw new CommandExecutionFailedException(command, occupation, dimension, direction);
    }
}
