/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.exception;

import com.mycompany.radio.car.simulator.enumeration.Command;
import java.util.Arrays;

/**
 *
 * @author emile
 */
public class CommandExecutionFailedException extends RuntimeException {
    private static final String MESSAGE = "Failed to execute command: ";
    
    public CommandExecutionFailedException(Command cmd, Object... args) {
        super(String.format("%s %s. args=[%s]", MESSAGE, cmd, Arrays.toString(args)));
    }
}
