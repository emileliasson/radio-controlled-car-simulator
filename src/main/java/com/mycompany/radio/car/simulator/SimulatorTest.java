/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator;

import com.mycompany.radio.car.simulator.car.RadioControlledCar;
import com.mycompany.radio.car.simulator.enumeration.Command;
import com.mycompany.radio.car.simulator.enumeration.Direction;
import com.mycompany.radio.car.simulator.environment.Occupasional;
import com.mycompany.radio.car.simulator.exception.InputValidationException;
import com.mycompany.radio.car.simulator.factory.EnvironmentFactory;
import com.mycompany.radio.car.simulator.factory.RadioControlledCarFactory;
import com.mycompany.radio.car.simulator.utils.Mapper;
import com.mycompany.radio.car.simulator.validator.GenericValidator;
import com.mycompany.radio.car.simulator.factory.ValidatorFactory;
import com.mycompany.radio.car.simulator.utils.CommandHelper;
import com.mycompany.radio.car.simulator.utils.RectangleUtils;
import com.mycompany.radio.car.simulator.utils.ValidatorUtils;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;


/**
 *
 * @author emile
 */
public class SimulatorTest {
    private static final String EXCEPTION_ILLEGAL_ARGUMENTS = "Input contains illegal argument(s).";
    private static final String EXCEPTION_ARGUMENTS_LENGTH = "Input contains illegal number of argument(s).";
    
    private static final String SPLIT_ON_SPACE = " ";
    private static final String SPLIT_ON_EVERY_CHARACTER = "";
    
    private final static Map<String, Direction> DIRECTION_MAPPER;
    private final static Map<String, Command> COMMAND_MAPPER;
    
    private final static GenericValidator<String> DIRECTION_VALIDATOR;
    private final static GenericValidator<String> COMMAND_VALIDATOR;
    private final static GenericValidator<String> NUMERIC_VALIDATOR;
    
    static {
        List<String> directionKeys = Arrays.asList("N","E","S","W");
        List<Direction> directionValues = Arrays.asList(Direction.values());
        List<String> commandKeys = Arrays.asList("F","R","B","L");
        List<Command> commandValues = Arrays.asList(Command.values());

        DIRECTION_MAPPER = Mapper.map(directionKeys, directionValues);
        COMMAND_MAPPER = Mapper.map(commandKeys, commandValues);

        DIRECTION_VALIDATOR = ValidatorFactory.makeAnyMatchValidator(String.class);
        DIRECTION_VALIDATOR.add(string -> DIRECTION_MAPPER.containsKey(string));
        
        COMMAND_VALIDATOR = ValidatorFactory.makeAnyMatchValidator(String.class);
        COMMAND_VALIDATOR.add(string -> COMMAND_MAPPER.containsKey(string));
        
        NUMERIC_VALIDATOR = ValidatorFactory.makeAllMatchValidator(String.class);
        NUMERIC_VALIDATOR.add(string -> ValidatorUtils.isNumeric(string));
        NUMERIC_VALIDATOR.add(string -> Integer.valueOf(string) >= 0);
    }
    
    
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in);) {
            
            Occupasional room;
            RadioControlledCar car;
            
            System.out.print("Enter the lenght and hight of the room, separeted by a blank space: ");
            String[] arguments = in.nextLine().split(SPLIT_ON_SPACE);
            validateRoomSize(arguments);
            
            room = EnvironmentFactory.makeRoomEnvironment2D(Integer.valueOf(arguments[0]), Integer.valueOf(arguments[1]));

            System.out.print("Enter car co-ordinates and direction, separeted by a blank space: ");
            arguments = in.nextLine().split(SPLIT_ON_SPACE);
            validateLocationAndDirection(arguments);
            
            car = RadioControlledCarFactory.makeMonsterTruck(DIRECTION_MAPPER.get(arguments[2]));
            room.occupy(car, RectangleUtils.rectangle(Integer.valueOf(arguments[0]), Integer.valueOf(arguments[1]), car.getDimension()));

            System.out.print("Enter a sequence of commands for the car: ");
            arguments = in.nextLine().split(SPLIT_ON_EVERY_CHARACTER);
            validateCommandSequence(arguments);
            
            CommandHelper cmdHelper = new CommandHelper(room.getOccupation(car), car.getDimension(), car.getDirection());
            Stream.of(arguments).map(arg -> COMMAND_MAPPER.get(arg)).forEach(cmd -> {
                cmdHelper.next(cmd);
                room.occupy(car, cmdHelper.getOccupation());
                printToConsole(cmd, cmdHelper); // printing to console
            });
        
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    static void validateRoomSize(String[] args) {
        if (args.length != 2)
            throw new InputValidationException(EXCEPTION_ARGUMENTS_LENGTH, Arrays.toString(args));
        else if (!NUMERIC_VALIDATOR.test(Arrays.asList(args)))
            throw new InputValidationException(EXCEPTION_ILLEGAL_ARGUMENTS, Arrays.toString(args));
    }
    
    static void validateLocationAndDirection(String[] args) {
        if (args.length != 3)
            throw new InputValidationException(EXCEPTION_ARGUMENTS_LENGTH, Arrays.toString(args));
        else if (!NUMERIC_VALIDATOR.test(Arrays.asList(args[0], args[1]))
                || !DIRECTION_VALIDATOR.test(args[2]))
            throw new InputValidationException(EXCEPTION_ILLEGAL_ARGUMENTS, Arrays.toString(args));
    }
    
    static void validateCommandSequence(String[] args) {
        if (!COMMAND_VALIDATOR.test(Arrays.asList(args)))
            throw new InputValidationException(EXCEPTION_ILLEGAL_ARGUMENTS, Arrays.toString(args));
    }

    static void printToConsole(Command command, CommandHelper commandHelper) {
        Rectangle2D rectangle = commandHelper.getOccupation();
        System.out.printf("%s --> Direction: %s, [x:%d, y:%d, widht:%d, height:%d]\n", 
                command, commandHelper.getDirection(), (int)rectangle.getX(), 
                (int)rectangle.getY(), (int)rectangle.getWidth(), (int)rectangle.getHeight());
    }
}
