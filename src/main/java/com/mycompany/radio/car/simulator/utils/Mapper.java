/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author emile
 */
public class Mapper {
    
    public static <K,V> Map<K,V> map(List<K> keys, List<V> values) {
        Map<K, V> mapper = new HashMap<>();
        for (int i = 0; i < keys.size(); i++) {
            mapper.put(keys.get(i), values.get(i));
        }
        return mapper;
    }
}
