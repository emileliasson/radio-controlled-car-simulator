/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.validator;

/**
 *
 * @author emile
 */
public class AllMatchValidator<T> extends GenericValidator<T> {
    
    @Override
    public boolean test(T object) {
        return getPreidacates()
                .parallelStream()
                .allMatch(predicate -> predicate.test(object));
    }
    
}
