/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.enumeration;

/**
 *
 * @author emile
 */
public enum Direction {
    NORTH, EAST, SOUTH, WEST;

    private final static int NR_OF_VALUES = values().length;
    private final static int CLOCKWISE_ROTATION = 1;
    private final static int ANTICLOCKWISE_ROTATION = NR_OF_VALUES - 1;
    
    
    public static Direction rotateClockwise(Direction currDir) {
        return rotate(currDir, CLOCKWISE_ROTATION);
    }
    
    public static Direction rotateAnticlockwise(Direction currDir) {
        return rotate(currDir, ANTICLOCKWISE_ROTATION);
    }
    
    private static Direction rotate(Direction currDir, int rotation) {
        return values()[(currDir.ordinal()+rotation) % NR_OF_VALUES];
    }
    
}
