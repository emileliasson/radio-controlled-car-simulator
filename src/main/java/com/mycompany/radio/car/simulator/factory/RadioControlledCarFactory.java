/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.factory;

import com.mycompany.radio.car.simulator.car.MonsterTruck;
import com.mycompany.radio.car.simulator.enumeration.Direction;

/**
 *
 * @author emile
 */
public class RadioControlledCarFactory {
    
    public static MonsterTruck makeMonsterTruck(Direction direction) {
        return new MonsterTruck(direction);
    }
}
