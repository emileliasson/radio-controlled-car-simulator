/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.environment;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author eliassone
 */
public abstract class Environment2D {
    private final Rectangle2D rectangle2D;
    
    public Environment2D(Rectangle2D rectangle2D) {
        this.rectangle2D = rectangle2D;
    }
    
    public boolean contains(Rectangle2D other) {
        return rectangle2D.contains(other);
    }
    
    public boolean contains(Point2D other) {
        return rectangle2D.contains(other);
    }
}
