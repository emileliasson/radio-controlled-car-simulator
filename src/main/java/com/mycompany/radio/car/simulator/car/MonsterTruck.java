/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.car;

import com.mycompany.radio.car.simulator.enumeration.Direction;
import java.awt.Dimension;

/**
 *
 * @author emile
 */
public class MonsterTruck extends RadioControlledCar {
    private static final int DEFAULT_WIDTH = 1;
    private static final int DEFAULT_HEIGHT = 1;
    
    public MonsterTruck(Direction direction) {
        super(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT), direction);
    }
}
