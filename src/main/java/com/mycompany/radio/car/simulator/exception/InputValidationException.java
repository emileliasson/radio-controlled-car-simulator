/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.exception;

/**
 *
 * @author eliassone
 */
public class InputValidationException extends RuntimeException {
    private static final String MESSAGE = "Input validation failed: ";
     
    public InputValidationException(String message, String input) {
        super(String.format("%s args=[%s]", MESSAGE, message, input));
    }
    
}
