/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.car;

import com.mycompany.radio.car.simulator.enumeration.Direction;
import java.awt.Dimension;
import java.util.Objects;

/**
 *
 * @author emile
 */
public abstract class RadioControlledCar {
    private final Dimension dimension;
    private Direction direction;

    public RadioControlledCar(Dimension dimension, Direction direction) {
        this.dimension = dimension;
        this.direction = direction;
    }
    
    public Dimension getDimension() {
        return dimension;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.dimension);
        hash = 67 * hash + Objects.hashCode(this.direction);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RadioControlledCar other = (RadioControlledCar) obj;
        if (!Objects.equals(this.dimension, other.dimension)) {
            return false;
        }
        if (this.direction != other.direction) {
            return false;
        }
        return true;
    }
}
