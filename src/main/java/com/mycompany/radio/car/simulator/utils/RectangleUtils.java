/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.utils;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author emile
 */
public class RectangleUtils {
    
    public static Rectangle rectangle(int width, int height, Dimension dimension) {
        return new Rectangle(new Point(width, height), dimension);
    }
}
