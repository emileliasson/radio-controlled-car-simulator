/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.factory;

import com.mycompany.radio.car.simulator.validator.AllMatchValidator;
import com.mycompany.radio.car.simulator.validator.AnyMatchValidator;

/**
 *
 * @author emile
 */
public class ValidatorFactory {
    
    public static <T> AllMatchValidator<T> makeAllMatchValidator(Class<T> type) {
        return  new AllMatchValidator<>();
    }
    
    public static <T> AnyMatchValidator<T> makeAnyMatchValidator(Class<T> type) {
        return  new AnyMatchValidator<>();
    }
    
}
