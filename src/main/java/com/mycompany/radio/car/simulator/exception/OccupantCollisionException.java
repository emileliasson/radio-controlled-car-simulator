/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.exception;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author emile
 */
public class OccupantCollisionException extends RuntimeException {
    public enum Collision { OCCUPANT, WALL };
    
    private static final Map<Collision, String> MESSAGE_MAPPER; 
    
    static {
        MESSAGE_MAPPER = Collections.unmodifiableMap(new HashMap<Collision,String>() {
            {
                put(Collision.OCCUPANT, "The cooupant has collided with another occupant.");
                put(Collision.WALL, "The occupant has collided with a wall");
            }
        });
    }
    
    public OccupantCollisionException(Collision type, Object... args) {
        super(String.format("%s args=[%s]", MESSAGE_MAPPER.get(type), Arrays.toString(args)));
    }
}
