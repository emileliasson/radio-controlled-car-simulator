/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.environment;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author emile
 */
public interface Occupasional<K ,V extends Rectangle2D> {
    void occupy(K occupant, V occupation);
    V getOccupation(K occupant);
    boolean unoccupy(K occupant);
}
