/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.validator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 *
 * @author emile
 */
public abstract class GenericValidator<T> implements Predicate<T> {

    private final Set<Predicate<T>> predicates = new HashSet<>();
    
    public Set<Predicate<T>> getPreidacates() {
        return predicates;
    }

    public boolean add(Set<Predicate<T>> predicates) {
        return this.predicates.addAll(predicates);
    }
    
    public boolean add(Predicate<T> predicate) {
        return predicates.add(predicate);
    }
    
    public boolean test(List<T> objects) {
        return objects.stream().allMatch(obj -> test(obj));
    }
}
