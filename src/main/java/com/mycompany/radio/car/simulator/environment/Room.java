/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.environment;

import com.mycompany.radio.car.simulator.exception.OccupantCollisionException;
import com.mycompany.radio.car.simulator.exception.OccupantNotFoundException;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author emile
 */
public class Room<K, V extends Rectangle2D> extends Environment2D implements Occupasional<K, V> {

    private final Map<K, V> occupations = new HashMap<>();

    public Room(Rectangle2D rectangle2D) {
        super(rectangle2D);
    }

    @Override
    public void occupy(K occupant, V occupation) {
        unoccupy(occupant);
        validate(occupation);
        occupations.put(occupant, occupation);
    }
    
    @Override
    public V getOccupation(K occupant) {
        V occupation = occupations.get(occupant);
        if (occupation == null)
            throw new OccupantNotFoundException(occupant);
        return occupation;
    }

    @Override
    public boolean unoccupy(K occupant) {
        return occupations.remove(occupant) == null ? false : true;
    }
    
    void validate(V occupation) throws OccupantCollisionException {
        if (!super.contains(occupation))
            throw new OccupantCollisionException(OccupantCollisionException.Collision.WALL, occupation);
        if (occupations.values().contains(occupation))
            throw new OccupantCollisionException(OccupantCollisionException.Collision.OCCUPANT, occupation);
    }
}
