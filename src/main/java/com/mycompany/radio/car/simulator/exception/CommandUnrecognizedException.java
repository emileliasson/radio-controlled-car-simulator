/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.exception;

import com.mycompany.radio.car.simulator.enumeration.Command;


/**
 *
 * @author emile
 */
public class CommandUnrecognizedException extends RuntimeException {
    private static final String MESSAGE = "Unrecognized command: ";
    
    public CommandUnrecognizedException(Command cmd) {
        super(MESSAGE + cmd);
    }
}
