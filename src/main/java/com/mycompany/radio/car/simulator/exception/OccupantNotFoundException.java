/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.exception;

/**
 *
 * @author emile
 */
public class OccupantNotFoundException extends RuntimeException {
    private static final String MESSAGE = "The occupant was not found.";
    
    public OccupantNotFoundException(Object occupant) {
        super(String.format("%s args=[%s]", MESSAGE, occupant.toString()));
    }
}
