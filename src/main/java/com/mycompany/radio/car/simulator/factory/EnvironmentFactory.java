/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.radio.car.simulator.factory;

import com.mycompany.radio.car.simulator.environment.Room;
import java.awt.Rectangle;

/**
 *
 * @author emile
 */
public class EnvironmentFactory {
    
    public static Room makeRoomEnvironment2D(int widht, int height) {
        return new Room(new Rectangle(widht, height));
    }
}
